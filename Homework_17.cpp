﻿#include <iostream>
#include <math.h>
using namespace std;

class Vector
{
private:
    double X;
    double Y;
    double Z;
public:
    double GetX()
    {
        cout << "X:";
        cin >> X;
        return X;
    }
    double GetY()
    {
        cout << "Y:";
        cin >> Y;
        return Y;
    }
    double GetZ()
    {
        cout << "Z:";
        cin >> Z;
        return Z;
    }
    double SetVector()
    {
        cout << "Enter 3D vector coordinates\n";
        X = GetX();
        Y = GetY();
        Z = GetZ();
        double vectorValue = sqrt(pow(X, 2) + pow(Y, 2) + pow(Z, 2));
        return vectorValue;
    }
};

int main()
{
    Vector newOne;
    cout << "The length of the vector is " <<newOne.SetVector();
    return 0;
}